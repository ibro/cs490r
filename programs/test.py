
var = input("Enter your name: ")


def say_hello(name):

    """
    Function: sayHello
    ------------------

    Displays Hello and the name entered

    name: a string containing a name

    returns: nothing

    """

    hello = "Hello " + name + "!"
    print(hello)

say_hello(var)
