import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.UIManager;

@SuppressWarnings("serial")
public class Main extends JFrame implements ActionListener {
	
	private MainPanel mp;
	private Model db;
	
	public Main() {
		
		db = new Model();
		JMenuBar mb = setupMenu();
		setJMenuBar(mb);
		
		mp = new MainPanel();
		setContentPane(mp);
		setPreferredSize(new Dimension(800, 600));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Parallel Coordinates");
		pack();
		setVisible(true);
	}
	
	private JMenuBar setupMenu() {
		//instantiate menubar, menus, and menu options
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("Table");
		JMenuItem marathon = new JMenuItem("Marathon");
		JMenuItem cis = new JMenuItem("CIS Students 2007 - 2012");
		JMenuItem cis2012 = new JMenuItem("CIS Students in 2012");
		JMenu editMenu = new JMenu("Edit");
		JMenuItem clearSelection = new JMenuItem("Clear Selections");

		//setup action listeners
		marathon.setActionCommand("marathon");
		cis.setActionCommand("cis");
		cis2012.setActionCommand("cis2012");
		marathon.addActionListener(this);
		cis.addActionListener(this);
		cis2012.addActionListener(this);
		clearSelection.addActionListener(e -> {
			mp.clearData();
			repaint();
		});
		//now hook them all together
			fileMenu.add(marathon);
			fileMenu.add(cis);
			fileMenu.add(cis2012);
			editMenu.add(clearSelection);

			menuBar.add(fileMenu);
			menuBar.add(editMenu);
			return menuBar;
		}

	public static void main(String[] args) {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e){}

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Main();
			}
		});
		
	}

	public static void say(String string) {
		System.out.println(string);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String table = e.getActionCommand();
		List<Axis> axes = db.performReconnaissanceQuery(table);
		int count = db.performFullQuery(table, axes);
		mp.setAxes(axes, count);
	}

}
