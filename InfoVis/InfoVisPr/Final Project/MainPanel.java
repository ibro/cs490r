import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SuppressWarnings("serial")
public class MainPanel extends JPanel {
	
	private int w,h;
	private List<Axis> axes;
	private int numEntities;
	private int leftMargin = 40;
	private Boolean dirty;
	
	public MainPanel() {
		super();
		axes = null;
		dirty = false;
	}
	
	private void prerender(Graphics g) {
		Graphics2D g1 = (Graphics2D)g;
		w = getWidth();
		h = getHeight();
		
		if (axes != null) {
			int xbuffer = (w-leftMargin*2) / (axes.size()-1);
			int ybuffer = 20;
			int x = leftMargin;
			for (Axis ax : axes) {
				Line2D dim = new Line2D.Float(x,ybuffer,x,h-ybuffer);
				ax.setDimensions(dim);
				x += xbuffer;
			}
//			PrintWriter writer = new PrintWriter("C:\\Users\\Ibrahim\\workspace\\ParallelCoordinatesFinal\\data.txt", "UTF-8");
			List<String> writer = new ArrayList<String>();
			writer.add("connect 'jdbc:derby:pollster;create=true';");
			writer.add("create table tennis (");
			writer.add("  point float,");
			writer.add("  emot_response varchar(20),");
			writer.add("  end_shot varchar(40),");
			writer.add("  outcome varchar(2)");
			writer.add(");");
			Path file = Paths.get("text.txt");
			for(int i = 0; i < axes.get(1).getValues().size(); i++) {
				System.out.println("Here");
				int response = new Random().nextInt(3-1+1)+1;
				String resp = "";
				if(response == 3) {
					resp = "positive";
				} else if(response == 2) {
					resp = "neutral";
				} else {
					resp = "negative";
				}
				int lastShot = new Random().nextInt(10-1+1)+1;
				String shot = "";
				switch(lastShot) {
					case 10:
						shot = "Forehand Winner";
						break;
					case 9:
						shot = "Backhand Winner";
						break;
					case 8:
						shot = "Forehand Volley Winner";
						break;
					case 7:
						shot = "Backhand Volley Winner";
					case 6:
						shot = "ACE";
						break;
					case 5:
						shot = "Opponent Unforced Error";
						break;
					case 4:
						shot = "Forehand Error";
						break;
					case 3:
						shot = "Backhand Error";
						break;
					case 2:
						shot = "Volley Error";
						break;
					case 1:
						shot = "Double Fault";
						break;
					default:
						shot = "Opponent Winner";
						break;
				}
				String outcome = "";
				if(lastShot >= 5) {
					outcome = "W";
				} else {
					outcome = "L";
				}
				writer.add("insert into tennis values("+(double)i+",'"+resp+"','"+shot+"','"+outcome+"');");
			}
			try {
				Files.write(file, writer, Charset.forName("UTF-8"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void paintComponent(Graphics g1) {
		Graphics2D g = (Graphics2D)g1;
		int width = getWidth();
		int height = getHeight();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);
		if(w != getWidth() || h != getHeight() || dirty) {

			prerender(g);
			
			dirty = false;
		}
		if(axes != null) {
			for(Axis ax : axes) {
				ax.draw(g);
			}
		}
	}

	public void clearData() {
		System.out.println("clearing");
	}

	public void setAxes(List<Axis> axes, int count) {
		this.axes = axes;
		dirty = true;
		this.numEntities = count;
		repaint();
	}
}
